FROM docker:cli AS docker-cli
FROM docker/buildx-bin:master AS buildx-bin

FROM python:3.11-alpine

COPY --from=docker-cli /usr/local/bin /usr/local/bin
COPY --from=buildx-bin /buildx /usr/libexec/docker/cli-plugins/docker-buildx

RUN apk add --no-cache  \
    bash~=5.2 \
    git~=2.45  \
    gpg~=2.4  \
    openssh~=9.7

COPY requirements.txt /
RUN pip3 install --no-cache-dir -r requirements.txt

COPY LICENSE.txt pipe.yml README.md /
COPY pipe /

ENTRYPOINT ["python", "/pipe.py"]
