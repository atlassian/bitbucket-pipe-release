import logging
import os
import sys

import pytest
from copy import copy, deepcopy
from unittest import TestCase

from pipe.pipe import Release, SCHEMA


class ReleaseTestCase(TestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog, mocker, capsys, fp):
        self.caplog = caplog
        self.mocker = mocker
        self.capsys = capsys
        self.fp = fp

    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())
        self.env_default = {
            'BITBUCKET_GIT_HTTP_ORIGIN': 'https://fake.git', 'BITBUCKET_BUILD_NUMBER': '0',
            'BITBUCKET_GIT_SSH_ORIGIN': 'git@fake.git',
            'BITBUCKET_APP_PASSWORD': 'test', 'BITBUCKET_USERNAME': 'test',
            'REGISTRY_USERNAME': 'test', 'REGISTRY_PASSWORD': 'test',
            'BITBUCKET_BRANCH': 'main', 'BITBUCKET_REPO_SLUG': 'repo',
            'IMAGE': 'test/test'
        }
        self.mocker.patch('pipe.pipe.Release.docker')
        self.mocker.patch('pipe.pipe.Release.docker.from_env')
        self.mocker.patch('pipe.pipe.Release._allow_executable')
        self.mocker.patch('pipe.pipe.Release.version_bump', return_value='0.1.0')
        self.mocker.patch('pipe.pipe.Release.generate_tags', return_value=['0.1.0', 'main'])
        self.mocker.patch('pipe.pipe.replace_content_by_pattern')
        self.mocker.patch('pipe.pipe.Release.git_commit')
        self.mocker.patch('pipe.pipe.Release.git_tag')

    def tearDown(self):
        sys.path = self.sys_path

    def test_default_success(self):
        self.mocker.patch.dict(os.environ, self.env_default)
        self.mocker.patch('pipe.pipe.Release.docker_release_default', return_value=None)

        pipe = Release(schema=SCHEMA, check_for_newer_version=True)

        with self.caplog.at_level(logging.DEBUG):
            pipe.run()

        out = self.capsys.readouterr().out

        self.assertEqual(pipe.DockerClientType.DOCKER, pipe.docker_client_type)
        self.assertIn("Generated tags", self.caplog.text)
        self.assertIn("✔ Release pipe finished successfully", out)

    def test_default_digest_success(self):
        test_env = deepcopy(self.env_default)
        test_env.update(
            {'WITH_DIGEST': 'True'}
        )
        self.mocker.patch.dict(os.environ, test_env)
        self.mocker.patch('pipe.pipe.Release.docker_release_default', return_value="sha256:abcdef")

        pipe = Release(schema=SCHEMA, check_for_newer_version=True)

        with self.caplog.at_level(logging.DEBUG):
            pipe.run()

        out = self.capsys.readouterr().out

        self.assertEqual(pipe.DockerClientType.DOCKER, pipe.docker_client_type)
        self.assertIn("Generated tags", self.caplog.text)
        self.assertIn("Replaced image version", self.caplog.text)
        self.assertIn("sha256:abcdef", self.caplog.text)
        self.assertIn("✔ Release pipe finished successfully", out)

    def test_docker_release_multiplatform_success(self):
        test_env = deepcopy(self.env_default)
        test_env.update(
            {'DOCKER_BUILD_PLATFORMS': 'linux/amd64,linux/arm64'}
        )
        self.mocker.patch.dict(os.environ, test_env)

        self.fp.register(
            'docker buildx create --name container-pipes --driver docker-container --use',
            stdout=["Creating docker container"])

        pipe = Release(schema=SCHEMA, check_for_newer_version=True)

        with self.caplog.at_level(logging.DEBUG):
            pipe.run()

        out = self.capsys.readouterr().out

        self.assertEqual(pipe.DockerClientType.BUILDX, pipe.docker_client_type)
        self.assertIn("Provided to build the next docker image platforms: ['linux/amd64', 'linux/arm64']", self.caplog.text)
        self.assertIn("Generated tags", self.caplog.text)
        self.assertIn("✔ Release pipe finished successfully", out)
