# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 5.8.0

- minor: Provide fix to Git dubious ownership by adding BITBUCKET_CLONE_DIR to safe directory.

## 5.7.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 5.7.0

- minor: Update pipe's base docker image to python:3.11-alpine.

## 5.6.1

- patch: Fix typo in the README.

## 5.6.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 5.5.1

- patch: Internal maintenance: Refactor semversioner call.
- patch: Internal maintenance: bump pipes versions in pipelines config file.

## 5.5.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 5.4.0

- minor: Update bitbucket-pipes-toolkit to fix vulnerability with certify.

## 5.3.2

- patch: Fix support SSH_KEY for a new release tagging.
- patch: Internal maintenance: Add support empty list of filenames to update.

## 5.3.1

- patch: Fix modified git settings. Add return back initial git configs predefined before pipe execution.

## 5.3.0

- minor: Add support for DOCKER_IMAGE_ARCHIVE_FILEPATH variable. Now the pipe supports docker load images from a tar archive file.

## 5.2.0

- minor: Update versions when docker image name is not equal repository slug.
- patch: Internal maintenance: update requirements.

## 5.1.1

- patch: Internal maintenance: Bump version for base docker image.

## 5.1.0

- minor: Add support for DOCKERFILE & CONTEXT variables

## 5.0.2

- patch: Updated readme example.

## 5.0.1

- patch: Internal maintenance: Update release workflow with pipe version 5.0.0.

## 5.0.0

- major: Add support REGISTRY_URL, REGISTRY_USERNAME, REGISTRY_PASSWORD variable. Now supported custom registries for your docker images. NOTE! Refactored auth variables to REGISTRY_USERNAME, REGISTRY_PASSWORD.

## 4.2.0

- minor: Bump semversioner vesion.
- patch: Improve semversioner logs: add source from where log came.

## 4.1.0

- minor: Bump bitbucket-pipes-toolkit -> 2.2.0.

## 4.0.1

- patch: Add category to pipe's metadata.

## 4.0.0

- major: Initial release: Pipe-release from internal to public.

## 3.2.2

- patch: Internal maintenance: fix regular expression to update version properly.

## 0.1.0

- minor: Pipe release.
