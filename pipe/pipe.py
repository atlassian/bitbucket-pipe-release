import glob
import re
import os
import subprocess
import stat
from enum import StrEnum

import docker
from python_on_whales import DockerClient
from semversioner.core import Semversioner
from semversioner.models import SemversionerException

from bitbucket_pipes_toolkit import Pipe


SCHEMA = {
    # GLOBALS
    'BITBUCKET_BRANCH': {'required': True, 'type': 'string'},
    'BITBUCKET_GIT_SSH_ORIGIN': {'required': True, 'type': 'string'},
    'BITBUCKET_GIT_HTTP_ORIGIN': {'required': True, 'type': 'string'},
    'BITBUCKET_REPO_SLUG': {'required': True, 'type': 'string'},
    'BITBUCKET_BUILD_NUMBER': {'required': True, 'type': 'integer'},

    # PIPE-SPECIFIC
    'IMAGE': {'required': True, 'type': 'string'},
    'REGISTRY_USERNAME': {'required': True, 'type': 'string'},
    'REGISTRY_PASSWORD': {'required': True, 'type': 'string'},
    'REGISTRY_URL': {'required': False, 'type': 'string'},
    'UPDATE_VERSION_IN_FILES': {'required': False, 'type': 'string', 'default': 'README.md pipe.yml'},
    'VERSION': {'required': False, 'type': 'string', 'default': None, 'nullable': True},
    'CHANGELOG': {'required': False, 'type': 'boolean', 'default': True},
    'GIT_PUSH': {'required': False, 'type': 'boolean', 'default': True},
    'GIT_USER': {'required': False, 'type': 'string', 'default': 'Bitbucket Pipelines Push Bot'},
    'SSH_KEY': {'required': False, 'type': 'string', 'default': None, 'nullable': True},
    'TAG': {'required': False, 'type': 'boolean', 'default': True},
    'WITH_DIGEST': {'required': False, 'type': 'boolean', 'default': False},
    'CONTEXT': {'required': False, 'type': 'string', 'default': '.'},
    'DOCKERFILE': {'required': False, 'type': 'string', 'default': './Dockerfile'},
    'DOCKER_IMAGE_ARCHIVE_FILEPATH': {'required': False, 'type': 'string', 'default': None, 'nullable': True},
    'DOCKER_BUILD_PLATFORMS': {'required': False, 'type': 'string', 'default': None, 'nullable': True},
}


def replace_content_by_pattern(filename, replacement_string, pattern):
    with open(filename) as f:
        content = f.read()

    with open(filename, 'w') as f:
        new_content = re.sub(re.compile(pattern), replacement_string, content)
        f.write(new_content)


class Release(Pipe):
    PIPE_CONTEXT_FILENAME = "pipe.yml"

    class DockerClientType(StrEnum):
        DOCKER = 'docker'
        BUILDX = 'buildx'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # resolve the build platform
        self.docker_client_type = self.DockerClientType.DOCKER
        if self.get_variable('DOCKER_BUILD_PLATFORMS') is not None:
            # https://docs.docker.com/build/building/multi-platform/
            self.build_platforms = self.get_variable("DOCKER_BUILD_PLATFORMS").split(',')
            self.log_info(f"Provided to build the next docker image platforms: {self.build_platforms}")

            self.docker_client_type = self.DockerClientType.BUILDX  # multi-platform build
            self.create_docker_driver_container()

        self._docker_client = None
        self.image = self.get_variable('IMAGE')

        self._script_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'scripts')
        self._allow_executable()

    def run(self):
        super().run()

        version = self.version_bump()
        tags = self.generate_tags(version)

        self.log_info(f"Generated tags {tags}")

        image_digest = self.docker_release(tags)

        if 'latest' in tags:
            tags.remove('latest')

        if self.get_variable('GIT_PUSH'):
            self.log_info(f'Committing version {version} to git')
            self.git_commit(version)
        if self.get_variable('WITH_DIGEST'):
            replace_content_by_pattern(self.PIPE_CONTEXT_FILENAME, f'{version}@{image_digest}', version)
            self.log_info(f'Replaced image version {self.image}:{version} with digest {image_digest} in file {self.PIPE_CONTEXT_FILENAME}')

        if self.get_variable('TAG') and self.get_variable('GIT_PUSH'):
            self.log_info(f'Tag version {version} to git')
            self.git_tag(version)

        self.success('Release pipe finished successfully')

    @property
    def docker(self):
        if self._docker_client is None:
            if self.docker_client_type == self.DockerClientType.BUILDX:
                self._docker_client = DockerClient()
            else:
                self._docker_client = docker.from_env()

        return self._docker_client

    def _allow_executable(self):
        for path in glob.iglob(os.path.join(self._script_path, '*.sh')):
            os.chmod(path, os.stat(path).st_mode | stat.S_IEXEC)

    def version_bump(self):
        """Bump a version.

        Bump a version, update necessary files, generate changelog
        """
        releaser = Semversioner()
        version = self.get_variable('VERSION')
        if not version:
            try:
                releaser.release()
                version = releaser.get_last_version()
            except SemversionerException as e:
                self.fail(f'Semversioner error: {e}')

        if self.get_variable('CHANGELOG'):
            self.log_info('Generating CHANGELOG.md file...')
            try:
                changelog = releaser.generate_changelog()
                with open('CHANGELOG.md', 'w') as f:
                    f.write(changelog)
            except SemversionerException as e:
                self.fail(f'Semversioner error: {e}')

        filenames_to_update = self.get_variable('UPDATE_VERSION_IN_FILES').strip()
        if filenames_to_update == '':
            return version

        for filename in filenames_to_update.split(' '):
            if filename == self.PIPE_CONTEXT_FILENAME:
                repo_slug = self.image.split('/')[-1]
            else:
                repo_slug = self.get_variable("BITBUCKET_REPO_SLUG")

            replace_content_by_pattern(filename, f'{repo_slug}:{version}',
                                       f'{repo_slug}:[0-9]*\.[0-9]*\.[0-9]*(@sha256:[a-fA-F0-9]*)?')  # noqa

        return version

    def generate_tags(self, version):
        branch = self.get_variable('BITBUCKET_BRANCH')
        if branch in ('master', 'main'):
            return ['latest', version]
        else:
            return [version]

    def docker_auth(self):
        login_params = {
            'username': self.get_variable('REGISTRY_USERNAME'),
            'password': self.get_variable('REGISTRY_PASSWORD'),
        }

        if self.docker_client_type == self.DockerClientType.BUILDX:
            if 'REGISTRY_URL' in self.variables:
                login_params['server'] = self.get_variable('REGISTRY_URL')
            self.docker.login(**login_params)
        else:
            if 'REGISTRY_URL' in self.variables:
                login_params['registry'] = self.get_variable('REGISTRY_URL')
            self.docker.api.login(**login_params)

    def docker_release(self, tags):
        if self.docker_client_type == self.DockerClientType.BUILDX:
            return self.docker_release_multi_platform(tags)

        return self.docker_release_default(tags)

    def docker_release_multi_platform(self, tags):
        self.docker_auth()

        self.log_info('Building multi-platform docker image...')

        full_tags = [f'{self.image}:{tag}' for tag in tags]
        for tag in full_tags:
            self.log_info(f'Tagged image {tag}')

        self.docker.build(
            context_path=self.get_variable("CONTEXT"),
            tags=full_tags,
            file=self.get_variable("DOCKERFILE"),
            platforms=self.build_platforms,
            progress='plain',
            push=True,
        )

        # TODO handle multiple digests for multi-platform build
        return None

    def create_docker_driver_container(self, name="container-pipes"):
        # https://docs.docker.com/build/drivers/docker-container/
        # https://docs.docker.com/build/building/multi-platform/
        COMMAND = f"docker buildx create --name {name} --driver docker-container --use"
        self.log_info('Creating docker container for multi-platform docker image build...')

        try:
            subprocess.run(COMMAND.split(" "), check=True, capture_output=True, text=True)
        except subprocess.CalledProcessError as exc:
            self.fail(f'{exc.output} {exc.stderr}')

    def docker_release_default(self, tags):
        self.docker_auth()

        docker_image_archive_path = self.get_variable("DOCKER_IMAGE_ARCHIVE_FILEPATH")
        if docker_image_archive_path:
            # Load an image that was previously saved using "docker save"
            if not os.path.exists(docker_image_archive_path):
                self.fail("DOCKER_IMAGE_ARCHIVE_FILEPATH does not exists.")

            self.log_info(f'Loading docker image from the file: {docker_image_archive_path}')
            with open(docker_image_archive_path, 'rb') as docker_image_tar_file:
                image = self.docker.images.load(docker_image_tar_file)[0]
        else:
            # Build an image
            self.log_info('Building docker image...')
            intermediate_tag = f'{self.image}:pipelines-{self.get_variable("BITBUCKET_BUILD_NUMBER")}'
            image, _ = self.docker.images.build(
                path=self.get_variable("CONTEXT"),
                tag=intermediate_tag,
                dockerfile=self.get_variable("DOCKERFILE"),
            )

        # NOTE: we take the digest of version last in a list pushed
        image_data = None
        for tag in tags:
            image.tag(self.image, tag=tag)
            self.log_info(f'Tagged image {self.image}:{tag}')

            output = self.docker.images.push(self.image, tag=tag, stream=True, decode=True)
            for line in output:
                self.log_info(line)
            image_data = self.docker.api.inspect_image(f'{self.image}:{tag}')

        return image_data['RepoDigests'][0].split('@')[-1]

    def git_commit(self, version):
        script = os.path.join(self._script_path, 'git-commit.sh')
        try:
            subprocess.run([script, version], check=True, capture_output=True, text=True)
        except subprocess.CalledProcessError as exc:
            self.fail(f'{exc.output} {exc.stderr}')

    def git_tag(self, version):
        script = os.path.join(self._script_path, 'git-tag.sh')
        try:
            subprocess.run([script, version], check=True, capture_output=True, text=True)
        except subprocess.CalledProcessError as exc:
            self.fail(f'{exc.output} {exc.stderr}')


def main():
    release = Release(schema=SCHEMA)
    release.run()


if __name__ == '__main__':
    main()
