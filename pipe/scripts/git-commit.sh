#!/usr/bin/env bash
#
# Configure git, add and commit changes, push
#
#

set -ex

git config --global --add safe.directory ${BITBUCKET_CLONE_DIR}

new_version=$1
USERNAME=${GIT_USER:-"Bitbucket Pipelines Push Bot"}

if [[ -n "${DEBUG}" ]]; then
  # check git
  echo "Debug info"
  ls -alt
  echo "Git directory: "
  ls -alt .git
  git --version
  git config --list
fi

# keep initial values provided by Bitbucket Pipelines service
HTTP_PROXY_DEFAULT=$(git config --get "http.${BITBUCKET_GIT_HTTP_ORIGIN}.proxy")
USERNAME_DEFAULT=$(git config --get user.name)
USER_EMAIL_DEFAULT=$(git config --get user.email)

git config user.name "$USERNAME"
git config user.email commits-noreply@bitbucket.org
echo "Committing updated files to the repository..."
git add .
git commit -m "Update files for new version '${new_version}' [skip ci]"


IDENTITY_FILE="/opt/atlassian/pipelines/agent/ssh/id_rsa_tmp"
KNOWN_HOSTS_FILE="/opt/atlassian/pipelines/agent/ssh/known_hosts"
mkdir -p ~/.ssh
chmod -R go-rwx ~/.ssh/


if [[ -n "${SSH_KEY}" ]]; then

  (umask  077 ; echo ${SSH_KEY} | base64 -d > ~/.ssh/git_private_key)
  chmod 600 ~/.ssh/git_private_key
  git remote set-url origin ${BITBUCKET_GIT_SSH_ORIGIN}
  GIT_SSH_COMMAND="ssh -i ~/.ssh/git_private_key  -o 'StrictHostKeyChecking=no'" git push -u origin ${BITBUCKET_BRANCH}

elif [[ -f "${IDENTITY_FILE}" ]]; then
  cp ${KNOWN_HOSTS_FILE} ~/.ssh/known_hosts
  cp ${IDENTITY_FILE} ~/.ssh/git_private_key
  chmod 600 ~/.ssh/git_private_key
  echo "IdentityFile ~/.ssh/git_private_key" >> ~/.ssh/config

  git remote set-url origin ${BITBUCKET_GIT_SSH_ORIGIN}
  git push -u origin ${BITBUCKET_BRANCH}

else
  git remote set-url origin "${BITBUCKET_GIT_HTTP_ORIGIN}"
  git config "http.${BITBUCKET_GIT_HTTP_ORIGIN}.proxy" http://host.docker.internal:29418/
  git push -u origin ${BITBUCKET_BRANCH}
  # set-up back a default value
  git config "http.${BITBUCKET_GIT_HTTP_ORIGIN}.proxy" ${HTTP_PROXY_DEFAULT}
fi

# set-up back default values
git config user.name "${USERNAME_DEFAULT}"
git config user.email "${USER_EMAIL_DEFAULT}"
